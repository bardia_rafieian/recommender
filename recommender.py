#we have filtered previous purchases form Box table in MongoDB, and also gathered nmber of box
#We have filtered season , ( just one season , or all seasons), and specify the cost(min, max).
#we updated the function which takes the frequency

import matplotlib.pyplot as plt
import numpy as np
import pickle
import csv
import pandas as pd
import sklearn as sk
from scipy.spatial.distance import squareform, pdist
import math
from scipy.spatial import distance
import seaborn as sb
import missingno as msno
import time
from sklearn.neighbors import NearestNeighbors
from sklearn.model_selection import train_test_split
from numpy.random import permutation
import random
from scipy.spatial import distance_matrix
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neighbors import NearestNeighbors as KNN
from itertools import islice
import collections
from pymongo import MongoClient  
from collections import defaultdict
import statistics
import ast
import cProfile, pstats, io


############to save log file of running time
pr = cProfile.Profile()
pr.enable()
# ... do something ...
#############end



########## start of calculating running time
start = time.time()
###########end


##########main function 
def rc_specified(user_input,N_ofBoxes,minimum_Prod,Maximum_Prod,season,min_cost,max_cost,freqDict):
    
    ############# to save the model as pickle we need to import pickle
    import pickle
    #############end
    #########the main dictionary to store recommended information
    dict1={}
    ############end
    #########accesss DB for tables   
    client = MongoClient('mongodb://165.227.4.123:13799',    #### to access to the mongoDB
                          username='viumeAdmin',
                          password='e34utc9-28',
                          #authSource='viume_db',
                          authMechanism='SCRAM-SHA-1'
                          )
    db = client['viume_db_2']    ##creating an instance of whole DB
    myorder_data=db['Orders']    ##### creating an instance of order table
    myproduct_data=db['Products']##### creating an instance of product table
    numberofbox=db['BOXES']    ##### creating an instance of boxes table
    ############end
    ######Define a path to our folders to read CSV files for numeric values of personal characteristics and product features
    # path_to_drive = "C://Users//Bardia//Desktop//VIUME//20190318_BBDD_AD//"
    path_to_drive = ""
    ################end
    
    ###############DETECTING NUMBER OF CURRENT BOX  
    num_current_box=numberofbox.find_one(sort=[("BOX_ID", -1)]) #finding the last number of box sorted 
    num_current_box=num_current_box.get("BOX_ID") 
    num_current_box=int(num_current_box)
    ##################end
    ##############################Creating new frequency dict based on the number of current BOX
    freqDict_n={}
    for category,freq in freqDict.items():
        freq=int(freq)
        #print("number of current box",num_current_box,"freq is",freq)
        if num_current_box%freq==0:
            freqDict_n.update({category:freq})            
    #print("New Freq dict",freqDict_n)
    ##############################Detecting previous recommended items from BOX table
    def filter_previous_products(user_input):#returns previous recommended products as a list
       previous_items=numberofbox.find({'USER_ID':user_input})
       A=previous_items.distinct('PRODUCT_ID')
       return A
    #############end
    ######################to filter the season for prducts
    #print(myorder_data)           
    ############
    previous_items=filter_previous_products(user_input)
    x_columns=[]#user table columns
    p_columns=[]#product table columns    
    List_of_similar_users=[]
    My_list_of_products_for_user=[]#list of products
    List_of_similar_products=[]#list to collect similar products to a product
    final_recommendation=[] # a final list of all recommended products
    mydata=pd.read_csv(path_to_drive+'New-User-Final-version-x-v4.csv',encoding = "ISO-8859-1") # reading the list of user characteristics
    product_dict=pd.read_csv(path_to_drive+'productDictionary.csv',encoding = "ISO-8859-1") #reading th list of product features 
    mydata.fillna(0,inplace=True) #filling missing values of personal characteristics table
    user_input=int(user_input)
    ################
    def check_user(user,df):
        result = df[df[("USER_ID")]==user].iloc[0]
        if result is not None:
            return True 
        else:
            return False    
    #############
    mydata = mydata.loc[:, (mydata != 0).any(axis=0)] #sort table
    np.round(mydata,decimals=2) #round the data
    mydata[mydata == 0] = 0.001 #make zero values to 0.001
    mytestx = mydata[mydata[("USER_ID")]==user_input].iloc[0] # puting the input user as the test format
    
    # The columns that we will be making predictions with.
    u_columns=(mydata.columns.values) #columns of prediction for KNN
    for value in u_columns:
        x_columns.append(value)
    #print(x_columns)
    x_columns.remove('Unnamed: 0') #remove unnamed column
    x_columns.remove('USER_ID') #remove user ID column
    
    # The column that we want to predict.
    
    train=mydata[x_columns] #train 
    
    train = (train - train.mean()) / train.std() #normalizing
    # Create the knn model.
    # Look at the five closest neighbors.
    user_knn = KNN(n_neighbors=20,algorithm='ball_tree') #using KNN ball tree algorithm for training
    # load the model on the training data.    
    user_knn.fit(train) # fit the train file    
    #Make point predictions on the test set using the loaded model.
    #Define the model
    AD_model = path_to_drive+'finalized_model-x.sav'
    #save the model
    pickle.dump(user_knn, open(AD_model, 'wb')) # save the model via pickle    
    ###################Product KNN   
    product_dict.fillna(0,inplace=True) #filling missing values of product table
    product_dict = product_dict.loc[:, (product_dict != 0).any(axis=0)] #sorting product table
    np.round(product_dict,decimals=2) #round the data
    up_columns=(product_dict.columns.values) #collecting columns for training
    for value in up_columns:
        p_columns.append(value)
    try:
        p_columns.remove('PRODUCT_ID')
        p_columns.remove('_id')
    except:
        pass
    # The column that we want to predict.
    
    p_train=product_dict[p_columns]   
    p_train = (p_train - p_train.mean()) / p_train.std()
    # Create the knn model.
    # Look at the five closest neighbors.
    product_knn = KNN(n_neighbors=20,algorithm='ball_tree')
    # load the model on the training data.    
    product_knn.fit(p_train) 
    #Define the model
    PRO_AD_model = path_to_drive+'product_finalized_model-x.sav'
    #save the model
    pickle.dump(product_knn, open(PRO_AD_model, 'wb'))
    ##################################search in order table    
    #User_and_products_df=pd.DataFrame()#columns="ID","Recommended Products")    
    #Load model for user similarity
    loaded_model = pickle.load(open(path_to_drive+'finalized_model-x.sav', 'rb'))
    
    mytestx = mytestx[x_columns]
    mytestx = (mytestx - mytestx.mean()) / mytestx.std()
    dist,idx=loaded_model.kneighbors([mytestx]) #dist is the distance and idx is the index number of nearest neighbor
    #print(dist,idx)
    ####iterating over the indexes 
    for i in idx:
        List_of_similar_users.append(mydata.loc[i,'USER_ID'].values)
    #List_of_similar_users=str(List_of_similar_users).replace(" ", "")
    #List_of_similar_users=List_of_similar_users.split(",")
    List_of_similar_users=List_of_similar_users[0]#keep first element 
    dist=dist.tolist()# to list!
    dist=dist[0]#keep first elements)
    try:
        List_of_similar_users.remove(user_input)
    except:
        pass
    dist=statistics.mean(dist) #calculating the mean of distances to keep it

    ################ one shot query for season filter from mongodb

    List_of_similar_users = [ str(x) for x in List_of_similar_users ]
    #print(List_of_similar_users)
    if season=="all":
        myproducts=myorder_data.find({ "USER_ID": {'$in':List_of_similar_users}})      
        myproducts=myproducts.distinct("PRODUCT_ID")
    else:
            try:
                season=int(season)
                myproducts=myorder_data.find({ "USER_ID": {'$in':List_of_similar_users},'SEASON_ID':season}) 
                myproducts=myproducts.distinct("PRODUCT_ID")
            except:
                print("error in season")
                myproducts=myorder_data.find({ "USER_ID": {'$in':List_of_similar_users}})      
                myproducts=myproducts.distinct("PRODUCT_ID")
                pass
    try:# removing previous recommended items for this user from recommended items 
     for x in myproducts:
        if x not in previous_items:
            My_list_of_products_for_user.extend([[str(x),dist]])
            dict1.update({str(x):int(dist)})
    except:
        print("Warning, no purchased products for this user!")

    #############################################
    final_recommendation.extend(My_list_of_products_for_user)#put ordered products to final list                
    #load model for product
    loaded_model_product = pickle.load(open(path_to_drive+'product_finalized_model-x.sav', 'rb'))
    # look for 5 similar products to each product in list of similar user products
    for item in My_list_of_products_for_user:
            try:
                j=item[1]
                i=int(item[0])
                mytesty = product_dict[product_dict[("PRODUCT_ID")]==i].iloc[0]
                #print("the ith: ",i,mytesty)
                mytesty = mytesty[p_columns]
                mytesty = (mytesty - mytesty.mean()) / mytesty.std()
                dist,idx=loaded_model_product.kneighbors([mytesty])
                for item in idx:
                    List_of_similar_products.append(product_dict.loc[idx,'PRODUCT_ID'].values)
                for i in List_of_similar_products[0]:
                    if item not in previous_items:
                        final_recommendation.extend([[str(i),j]])
                        dict1.update({str(i):j})
                List_of_similar_products.clear()
            except:
                pass

    My_list_of_products_for_user.extend(final_recommendation) #extendind the list of similar users 

    
    if len(dict1)<20: # in this case, if the number of products are not enough, we fill the dictionary with category choice
        print("Warning: Not enough products for this user,Goes to Random AI stylist descision")
        products_fornull = myproduct_data["PRODUCT_ID"].values
        #print("pp is ",products_fornull)
        title_fornull=myproduct_data["FAMILIA"].values
        for i in range(20):
            key=random.choice(products_fornull)
            key=float(key)
            key=int(key)
            value=random.choice(title_fornull)
            dict1.update({str(key):4.5})

    #############
    ##########function to return all products with COST FILTER, returns a dictionary and a list for next usages
    def probability3(user,min_cost,maxcost):
        new_dict={}

        list_of_products = list(dict1.keys())
        temp_products = [ str(x) for x in list_of_products ]
        print("test-1",len(temp_products)) 
        filtered_cost_prducts=myproduct_data.find({
       "PVP": {
           "$gt": int(min_cost),
           "$lt": int(maxcost)
                   }, 
       "PRODUCT_ID": {'$in':temp_products}
       })
       
        filtered_cost_prducts=filtered_cost_prducts.distinct("PRODUCT_ID")
        print("test1",len(filtered_cost_prducts))
        for key,value in dict1.items():
            if key in filtered_cost_prducts :
                new_dict.update({str(key):value})
        my_list_ofproducts = list(new_dict.keys())
        List_of_similar_users = [ str(x) for x in my_list_ofproducts ] 
        new_dict2={int(user_input):new_dict}
        return new_dict2,List_of_similar_users     
    ############################################Create DF of dictionary
    #here we create a df of dictionary with categories and their products, so we can supervise them easier   
    myrecommend,mylist_p=probability3(user_input,min_cost,max_cost)

    
    #finding the title and color of products and save them in a dict
    title=myproduct_data.find({'PRODUCT_ID' :{'$in':mylist_p}})
    title2={}
    Id_color={}
    for item  in title:######create dictionary with p_id and category 
        title2.update({str(item['PRODUCT_ID']):str(item['FAMILIA'])})
        Id_color.update({str(item['PRODUCT_ID']):str(item['COLOR_AGRUPADO'])})
        
    ##########To reverse and save key vlues of dictionary as a proper type for Dataframe
    
    res = {} 
    for key, value in title2.items(): 
        if value in res: 
            res[value].append(key) 
        else: 
           res[value]=[key] 
    #creating a dataframe including categories as column names and IDs as column values
    Rdataframe=pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in res.items() ]))
    #Rdataframe.to_csv("C://Users//Bardia//Desktop//VIUME//20190318_BBDD_AD//Rdataframe(new).csv") #saving the Dataframe to the file

    ###################################
    #this function creates weights based on the distances
    '''
    items=[]
    weights=[]
    w_dict={}        
    for item, value in myrecommend.items():
        for y,z in value.items():
            items.append(y)
            weights.append(z)
            w_sum=sum(weights)
    for key,value in myrecommend.items():
        for y,z in value.items():
            w_dict.update({y:(int(abs(z/w_sum-1)*10000))})
    '''
    ############### THIS function returns the most probable items in a list based on weights
    '''
    def genLootFromTable(count=2,table=None):# reads items and weights from a dictionary and calculates the probability happening of each item in a order
                if table:
                    tmp = []
                    out = []
                    
                    for item in table:
                       itm = table[item]
                       t = [ item ] * itm
                       tmp += t
                    
                    for c in range(count):
                        out += [random.choice(tmp)]
                    
                    return out
    #print("seventh step") 
    '''
    ################STYLIST function based on the flowchart attached in the JIRA

    StylistDataFrame=pd.read_csv(path_to_drive+"Stylist.csv") #reading stylist from a csv file

    #myproduct_data
    def Stylist(freq_dict,stylist_df,dtframe,minimum_Prod,Maximum_Prod):#freq_dict is the products which should be available in a box, stylist_df is the dataframe of styles based on the number of products,dtframe is the dataframe of recommended products, min and maximum products is the number of products which should be available in a Box
        #set_freq=0
        amazing=[]
        if Maximum_Prod>minimum_Prod:########## check minimum and maximum number of products
            n_prod=random.randrange(minimum_Prod,Maximum_Prod)
    
        else:
            n_prod=minimum_Prod

        ############### Look for the right Stylist in terms of number ofproducts in each style
        Stylist_array=[]
        if n_prod==5:
            Stylist_dict=stylist_df['STYLES5']      
            for style in Stylist_dict:
                Stylist_array.append(style)
            #print(Stylist_array)
        elif n_prod==4:
            Stylist_dict=stylist_df['STYLES4']      
            for style in Stylist_dict:
                Stylist_array.append(style)
        elif n_prod==3:
            Stylist_dict=stylist_df['STYLES3']      
            for style in Stylist_dict:
                Stylist_array.append(style)
        elif n_prod==2:
            Stylist_dict=stylist_df['STYLES2']      
            for style in Stylist_dict:
                Stylist_array.append(style)
        else:
            print("error with wrong number of products")
        table_number=0
        #try:
        print(len(stylist_df))
        for x in Stylist_array:
            #print("freq dict is :",freq_dict.keys())
            x=ast.literal_eval(x)
            print("stylist is ",x.keys())
            table_number+=1
            if all(k in x for k in freq_dict.keys()):
                print("found freq match with stylist match")
                #set_freq=set(freq_dict.keys())
                #set_style=set(x.keys())
                #myset=set_style.difference(set_freq)
                for category,color in x.items():
                    try:
                        temp_df=Rdataframe[category]
                        print("test1")
                        temp_df=temp_df.dropna()
                        temp_df=temp_df.values
                        temp_df = list(filter(None,temp_df))
                        temp_len=len(temp_df)#the length of each category in dataframe 
                        counter=0 #to count the read items in dataframe and compare with temp_len
                        for y in temp_df:
                            y=int(y)
                            counter+=1
                            temp_color=Id_color[str(y)]
                            print("temp color issssssssssssss",temp_color,y)
                            if temp_color==color:
                                amazing.append([category,int(y)])
                                print("could found category match and color match",int(y))
                                temp_color=[]
                                break
                            else:
                                print("could not find color match, but found category match","\n","total count is: ",temp_len,"current count is: ",counter)
                        if(counter==temp_len):
                                print("the lenght is passd but not found related match")
                                amazing=[]
                                temp_color=[]
                                break   
                        temp_len=0
                        counter=0
                        temp_color=[]
                    except:
                        print("could not find category match")
                        pass
                if(len(amazing)==len(x)):
                    print("we have reached to the correct finding of frequent dict and stylist dict with color match",amazing,"in the :",table_number,"table")
                    break
        else:#if we coul'd not find the freq dict items in current  stylist table
                print("#freq dict was not available in the",table_number,"table of stylist tables")
                print('test2')
        ###############################################################################
        
        #except:#end of the loop then we will just look for the category
        #try:
                amazing=[]
                print("test3")
                print("loop finished and no match is not found in any stylist table with color match, we go just fo the category matching")
                #Now we should work on top and down  tags on product table
                table_number=0
                for x in Stylist_array:
                    
                    x=ast.literal_eval(x)
                    table_number+=1
                    if all(k in x for k in freq_dict.keys()):
                        print("found freq match with stylist match")
                        #set_freq=set(freq_dict.keys())
                        #set_style=set(x.keys())
                        #myset=set_style.difference(set_freq)
                        for category,color in x.items():
                            try:
                                counter+=1
                                temp_df=Rdataframe[category]
                                temp_df=temp_df.dropna()
                                temp_df=temp_df.values
                                temp_df = list(filter(None,temp_df))
                                temp_len=len(temp_df)#the length of each category in dataframe 
                                myrandom=random.choice(temp_df)
                                amazing.append([category,int(y)])
        
                            except:
                                print("could not find category match1")
                                pass
                            if(counter==temp_len):
                                    print("the lenght is passd but not found related match")
                                    amazing.clear()
                                    temp_color=[]
                                    break 
                            temp_len=0
                            counter=0
                            temp_color=[]
                        if(len(amazing)==len(x)):
                            print("JUST CATEGORY MODE: we have reached to the correct finding of frequent dict and stylist dict ",amazing,"in the :",table_number,"table")
                            break
                    else:#if we coul'd not find the freq dict items in current  stylist table
                        print("freq dict was not available in the",table_number,"table of stylist tables, We go for the priorities")                     
                        
                else:
                    print("Could not find even catergory match, should return based on the priorities")
                    amazing=[]
                    history_category=[]#### to store list of categories which already filled in the box 
                    if(len(freq_dict))>0: ########## if there is something in the frequent dictionary, means the B2B defined something to be available in this current Box              
                        while(len(amazing)<=(n_prod+1)):
                            for category,freq in freq_dict.items():
                              try:                                 
                                history_category.append(category)
                                temp_df=Rdataframe[category]
                                temp_df=temp_df.dropna()
                                temp_df=temp_df.values
                                temp_df = list(filter(None,temp_df))
                                temp_len=len(temp_df)#the length of each category in dataframe 
                                myrandom=random.choice(temp_df)
                                amazing.append([category,int(y)])
                                temp_df=[]
                              except:
                                  print("the category is not available on Rdataframe")
                                  break
                             ####at this step, the box is covered all freq dict items, but not filled completely
                            if(len(amazing)<=(n_prod)):
                                while(len(amazing)<=(n_prod+1)):
                                    try:
                                         list_of_categories=list(Rdataframe.columns.values)
                                         print(list_of_categories)
                                         print(history_category)
                                         list_of_categories2 = [x for x in list_of_categories if x not in history_category]
                                         random_category=str(random.choice(list_of_categories2))
                                         history_category.append(random_category)
                                         temp_df=Rdataframe[random_category]
                                         temp_df=temp_df.dropna()
                                         temp_df=temp_df.values
                                         temp_df = list(filter(None,temp_df))
                                         temp_len=len(temp_df)#the length of each category in dataframe 
                                         myrandom=random.choice(temp_df)
                                         amazing.append(int(myrandom))
                                         temp_df=[]
                                         list_of_categories=[]
                                         if(len(amazing)==(n_prod)):
                                             #print("we have filled the box with just the priorities")
                                             break
                                    except:
                                       print("could not find any other category in Rdataframe")
                                       amazing=[]
                                       history_category=[]
                                       pass
                            if(len(amazing)==(n_prod)):
                                             print("we have filled the box with just the priorities")
                                             break
                    else:####means there is nothing predefined in the B2B dictionary and there is not match with stylist(seems the worst situation), so we have to select something(also there is no stylist availbale for this Dataframe of recommended products. This means the number of products are not enough for stylist!)
                        while(len(amazing)<=(n_prod)):
                            try:
                                     list_of_categories2 = [x for x in list_of_categories if x not in history_category]
                                     random_category=str(random.choice(list_of_categories2))
                                     history_category.append(random_category)
                                     temp_df=Rdataframe[random_category]
                                     temp_df=temp_df.dropna()
                                     temp_df=temp_df.values
                                     temp_df = list(filter(None,temp_df))
                                     temp_len=len(temp_df)#the length of each category in dataframe 
                                     myrandom=random.choice(temp_df)
                                     amazing.append([category,int(y)])
                                     temp_df=[]
                                     list_of_categories=[]
                                     if(len(amazing)==n_prod or len(amazing)==(n_prod-1)):
                                         print("we have filled the box with just the random selection of categories")
                                         break
                            except:
                                   print("the Rdataframe does not have enough products and categories to fill a box")
                                   amazing=[]
                                   history_category=[]
                                   pass                           
        
                print("numebr of products are: ",n_prod)
                #print(freq_dict.items())
        return amazing
    '''
    ###############################################Replacing Stylist instead of old one#############################################
    FINISH
    '''
    recommend=Stylist(freqDict_n,StylistDataFrame,Rdataframe,minimum_Prod,Maximum_Prod)
    #print("New Freq dict",freqDict_n)            
    ##########except
        #array_box.clear()
                
    #recommend=return_random_box(user_input,N_ofBoxes,minimum_Prod,Maximum_Prod)
    #print("should finish here")
    #end = time.time()
    #print('the process time is :' ,(end - start)/60 ,'minutes')
    #Save the results in array format
    #print(myfularray)
    return recommend
#to test       
#print(mostrecommendded(22))

A=[14]
w=open('results.txt','w')
items={'BISUTERIA':'2','CAMISA/BLUSA/TOP':'6','PUNTO':'1'}
for x in A:
    B=rc_specified(x,3,5,6,'all',10,500,{'BISUTERIA':'2','CAMISA/BLUSA/TOP':'4'})    
    print(B)
            
end = time.time()
print('the process time is :' ,((end - start)) ,'seconds')            
       # print(allrecommended(item))#w.close()
pr.disable()
s = io.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
mytime=s.getvalue()
w2=open('time_results.txt','w')
w2.write(mytime)
w2.close()
#print(s.getvalue())

#Note: recommend format is :   {boxno:{category:[pID,score],catgory:[pID,score],...},boxno:{...}}
#3097